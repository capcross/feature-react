import React from 'react';
import PropTypes from 'prop-types';
// import { View, TouchableHighlight } from 'react-native';
// import { Container, Content, Header, Body, Title, Button, Left, Icon, Right } from 'native-base';
import bindActionCreators from 'redux/lib/bindActionCreators';
import connect from 'react-redux/lib/connect/connect';
import compose from 'recompose/compose';
import pure from 'recompose/pure';

import { reduxForm } from 'redux-form';

import withBackButton from '../components/common/hoc/withBackButton';

import * as TaskActions from '../redux/task/taskActions';
import getVisibleTasks from '../redux/task/taskSelector';

import FilterForm from '../components/filter/FilterForm';

import ModalWaitBar from '../components/common/WaitBar';
import Util from '../components/common/util/Util';

const mapStateToProps = state => ({
  items: getVisibleTasks(state),
  filter: state.task.filter,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(TaskActions, dispatch),
});

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'filterForm',
  }),
  withBackButton(),
  pure,
);

@enhance
export default class FilterScreen extends React.Component {
  static propTypes = {
    filter: PropTypes.object.isRequired,
    navigation: PropTypes.object.isRequired,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    handleSubmit: PropTypes.func.isRequired,
  };

  getStyles = () => ({
    content: {
      justifyContent: 'space-between',
      padding: 8,
    },
    header: {
      backgroundColor: '#455A64',
    },
    title: {
      color: '#FFFFFF',
    },
    icon: {
      color: '#FFFFFF',
    },
    doneButton: {
      backgroundColor: 'rgba(231,76,60,1)',
      borderColor: 'rgba(231,76,60,1)',
      borderWidth: 1,
      height: 56,
      width: 56,
      borderRadius: 28,
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      bottom: 30,
      right: 30,
      shadowColor: '#000000',
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 0,
      },
    },
    doneButtonIcon: {
      color: 'white',
    },
  });

  handleChange = () => console.log('FilterScreen: change!!!');

  // handleSubmit = () => {
  //   console.log('FilterScreen.handleSubmit()');
  //   return this.props.handleSubmit(this.submitTask);
  // };
  handleSubmit = () => this.props.handleSubmit(this.submitTask);

  goBack = () => this.props.navigation.goBack();

  submitTask = (values) => {
    console.log(`FilterScreen.submitTask(${JSON.stringify(values)})`);
    this.props.actions.changeFilter({
      id: values.id,
      author: values.author,
      name: values.name,
      nameEn: values.nameEn,
      description: values.description,
    });

    this.props.navigation.goBack();
  };

  render() {
    const styles = this.getStyles(this.props);

    return (
      // <Container>
      //   <Header style={styles.header}>
      //     <Left>
      //       <Button onPress={this.goBack} transparent>
      //         <Icon name="arrow-back" style={styles.icon} />
      //       </Button>
      //     </Left>
      //     <Body>
      //       <Title style={styles.title}>Фильтр поиска</Title>
      //     </Body>
      //     <Right />
      //   </Header>
      //   <Content contentContainerStyle={styles.content}>
      //     <FilterForm
      //       filter={this.props.filter}
      //       onSubmit={this.handleSubmit()}
      //       onChange={this.handleChange()}
      //     />

      //     <ModalWaitBar
      //       ref={(c) => {
      //         this.waitBar = c;
      //       }}
      //     />
      //   </Content>
      //   {Util.platformOS() === 'android' && (
      //     <View>
      //       <TouchableHighlight
      //         style={styles.doneButton}
      //         underlayColor="red"
      //         onPress={this.handleSubmit()}
      //       >
      //         <Icon name="md-checkmark" style={styles.doneButtonIcon} />
      //       </TouchableHighlight>
      //     </View>
      //   )}
      // </Container>
      <div>FilterScreen</div>
    );
  }
}
