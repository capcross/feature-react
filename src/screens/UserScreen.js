import React from 'react';
import PropTypes from 'prop-types';
//import { Container, Content, Header, Body, Title, Button, Left, Icon, Right } from 'native-base';
import bindActionCreators from 'redux/lib/bindActionCreators';
import connect from 'react-redux/lib/connect/connect';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import { reduxForm } from 'redux-form';

import UserDetail from '../components/user/UserDetail';
import * as UserActions from '../redux/user/userActions';
//import { logoutThunk } from '../redux/user/userMiddleware';

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(UserActions, dispatch),
});

const enhance = compose(
  connect(null, mapDispatchToProps),
  reduxForm({ form: 'userForm' }),
  pure,
);

@enhance
export default class UserScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  getStyles = () => ({
    content: {
      justifyContent: 'space-between',
      padding: 8,
    },
    inputGroup: {
      flex: 0.9,
    },
    header: {
      backgroundColor: '#455A64',
    },
    title: {
      color: '#FFFFFF',
    },
    icon: {
      color: '#FFFFFF',
      fontSize: 25,
    },
    editButton: {
      backgroundColor: 'rgba(231,76,60,1)',
      borderColor: 'rgba(231,76,60,1)',
      borderWidth: 1,
      height: 56,
      width: 56,
      borderRadius: 28,
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      bottom: 30,
      right: 30,
      shadowColor: '#000000',
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 0,
      },
    },
    editButtonIcon: {
      color: 'white',
    },
  });

  logout = () => {
    this.props
      // eslint-disable-next-line react/prop-types
      // .dispatch(
      //   logoutThunk(),
      // )
      // .then(() => {
      //   console.log('UserScreen.logout(): logout Done!');
      // })
      // .catch((err) => {
      //   console.log(err);
      // });
      console.log('UserScreen.logout(): logout Done!');
  };

  render() {
    // const { user } = this.props.navigation.state.params;
    // Получить из state, а в state - по запросу с backend 
    const user = {
      id: "216",
      name: "Alexander",
    };

    const styles = this.getStyles();

    return (
      // <Container>
      //   <Header style={styles.header}>
      //     <Left>
      //       <Button onPress={() => {this.props.navigation.openDrawer()}} transparent>
      //         <Icon name="menu" style={styles.icon} />
      //       </Button>
      //     </Left>
      //     <Body>
      //       <Title style={styles.title}>Профиль</Title>
      //     </Body>
      //     <Right>
      //       <Button
      //         onPress={() => {
      //           this.logout();
      //         }}
      //         transparent
      //       >
      //         <Icon name="exit" style={styles.icon} />
      //       </Button>
      //     </Right>
      //   </Header>
      //   <Content contentContainerStyle={styles.content}>
      //     <UserDetail
      //       navigation={this.props.navigation}
      //       user={user}
      //     />
      //   </Content>
      // </Container>
      <div>UserScreen</div>
    );
  }
}
