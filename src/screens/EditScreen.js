import React from 'react';
import PropTypes from 'prop-types';
// import { View, TouchableHighlight } from 'react-native';
// import { Container, Content, Header, Body, Title, Button, Left, Icon, Right } from 'native-base';
import bindActionCreators from 'redux/lib/bindActionCreators';
import connect from 'react-redux/lib/connect/connect';
import compose from 'recompose/compose';
import pure from 'recompose/pure';

import { reduxForm } from 'redux-form';

import withBackButton from '../components/common/hoc/withBackButton';

import * as TaskActions from '../redux/task/taskActions';
import EditForm from '../components/edit/EditForm';
import { updateTaskThunk } from '../redux/task/taskMiddleware';
import Util from '../components/common/util/Util';

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(TaskActions, dispatch),
});

const mapStateToProps = (state) => {
  return {
    initialValues: state.task.activeItem,
  }
}

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'editForm',
  }),
  withBackButton(),
  pure,
);

@enhance
export default class EditScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
  };

  getStyles = () => ({
    content: {
      justifyContent: 'space-between',
      padding: 8,
    },
    header: {
      backgroundColor: '#455A64',
    },
    title: {
      color: '#FFFFFF',
      width: 200,
    },
    icon: {
      color: '#FFFFFF',
    },
    doneButton: {
      backgroundColor: 'rgba(231,76,60,1)',
      borderColor: 'rgba(231,76,60,1)',
      borderWidth: 1,
      height: 56,
      width: 56,
      borderRadius: 28,
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      bottom: 30,
      right: 30,
      shadowColor: '#000000',
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 0,
      },
    },
    doneButtonIcon: {
      color: 'white',
    },
  });

  handleSubmit = () => this.props.handleSubmit(this.submitTask);

  goBack = () => this.props.navigation.goBack();

  submitTask = (values) => {
    console.log(`EditScreen.submitTask(): values = ${JSON.stringify(values)}`);

    // TODO Проверять заполнение обязательных полей
    // ...

    this.props
      // eslint-disable-next-line react/prop-types
      .dispatch(
        updateTaskThunk({
          id: this.props.initialValues.id,
          author: values.author,
          name: values.name,
          nameEn: values.nameEn,
          description: values.description,
          statusCode: values.statusCode,
        }),
      )
      .then(() => {
        console.log('EditScreen.submitTask(): updateTask Done!');
        this.props.navigation.navigate('Home'); // TODO заменить времянку. Нужен возврат в DetailScreen - goBackAndRefresh().
      })
      .catch((err) => {
        console.log(err);
      });

    console.log('EditScreen.submitTask(): AFTER this.props.dispatch');
  };

  render() {
    const task  = this.props.initialValues;
    console.log(`EditScreen.render(): task = ${JSON.stringify(task)}`);
    console.log(`EditScreen.render(): author = ${task.author}`);

    const styles = this.getStyles(this.props);

    return (
      // <Container>
      //   <Header style={styles.header}>
      //     <Left>
      //       <Button onPress={this.goBack} transparent>
      //         <Icon name="arrow-back" style={styles.icon} />
      //       </Button>
      //     </Left>
      //     <Body>
      //       <Title style={styles.title}>Редактирование</Title>
      //     </Body>
      //     <Right />
      //   </Header>
      //   <Content contentContainerStyle={styles.content}>
      //     <EditForm
      //       initialValues={task}
      //     />
      //   </Content>
      //   {Util.platformOS() === 'android' && (
      //     <View>
      //       <TouchableHighlight
      //         style={styles.doneButton}
      //         underlayColor="red"
      //         onPress={this.handleSubmit()}
      //       >
      //         <Icon name="md-checkmark" style={styles.doneButtonIcon} />
      //       </TouchableHighlight>
      //     </View>
      //   )}

      // </Container>
      <div>EditScreen</div>
    );
  }
}
