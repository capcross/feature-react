import React from 'react';
import PropTypes from 'prop-types';
// import { View, TouchableHighlight } from 'react-native';
// import { Container, Content, Header, Body, Title, Button, Left, Icon, Right } from 'native-base';
import bindActionCreators from 'redux/lib/bindActionCreators';
import connect from 'react-redux/lib/connect/connect';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import { reduxForm } from 'redux-form';

import ViewDetail from '../components/view/ViewDetail';
import withBackButton from '../components/common/hoc/withBackButton';
import * as TaskActions from '../redux/task/taskActions';
import { logoutThunk } from '../redux/task/taskMiddleware';
import Util from '../components/common/util/Util';

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(TaskActions, dispatch),
});

const mapStateToProps = (state) => {
  return {
    task: state.task.activeItem,
  }
}

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withBackButton(),
  pure,
);

@enhance
export default class DetailScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  componentWillReceiveProps(nextProps) {
    console.log(`DetailScreen.componentWillReceiveProps: nextProps = ${nextProps}`);
    console.log(
      `DetailScreen.componentWillReceiveProps: this.props.navigation.state.params = ${this.props
        .navigation.state.params}`,
    );
    // if (nextProps.count != this.props.count) {
    //     this.props.navigation.setParams({ count: nextProps.count });
    // }
  }

  getStyles = () => ({
    content: {
      justifyContent: 'space-between',
      padding: 8,
    },
    inputGroup: {
      flex: 0.9,
    },
    header: {
      backgroundColor: '#455A64',
    },
    title: {
      color: '#FFFFFF',
    },
    icon: {
      color: '#FFFFFF',
      fontSize: 25,
    },
    editButton: {
      backgroundColor: 'rgba(231,76,60,1)',
      borderColor: 'rgba(231,76,60,1)',
      borderWidth: 1,
      height: 56,
      width: 56,
      borderRadius: 28,
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      bottom: 30,
      right: 30,
      shadowColor: '#000000',
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 0,
      },
    },
    editButtonIcon: {
      color: 'white',
    },
  });

  removeTask = () => {
    this.props
      // eslint-disable-next-line react/prop-types
      .dispatch(
        logoutThunk({
          id: this.props.task.id,
        }),
      )
      .then(() => {
        console.log('DetailScreen.removeTask(): removeTask Done!');
        this.props.navigation.goBack()
      })
      .catch((err) => {
        console.log(err);
      });
  };

  goBack = () => this.props.navigation.goBack();

  goToUpdateTask = () => {
    const task = this.props.task;
    console.log(`goToUpdateTask(${task})`);
    this.props.navigation.navigate('EditTask');
  };

  render() {
    const task = this.props.task;
    const styles = this.getStyles(this.props);

    return (
      // <Container>
      //   <Header style={styles.header}>
      //     <Left>
      //       <Button onPress={this.goBack} transparent>
      //         <Icon name="arrow-back" style={styles.icon} />
      //       </Button>
      //     </Left>
      //     <Body>
      //       <Title style={styles.title}>Задача</Title>
      //     </Body>
      //     <Right>
      //       <Button
      //         onPress={() => {
      //           //                                task.remove(); action->reducer
      //           this.removeTask(task);
      //         }}
      //         transparent
      //       >
      //         <Icon name="trash" style={styles.icon} />
      //       </Button>
      //     </Right>
      //   </Header>
      //   <Content contentContainerStyle={styles.content}>
      //     <ViewDetail
      //       navigation={this.props.navigation}
      //       task={task}
      //     />
      //   </Content>
      //   {Util.platformOS() === 'android' && (
      //     <View>
      //       <TouchableHighlight
      //         style={styles.editButton}
      //         underlayColor="red"
      //         onPress={() => { this.goToUpdateTask(); }}
      //       >
      //         <Icon name="md-create" style={styles.editButtonIcon} />
      //       </TouchableHighlight>
      //     </View>
      //   )}
      // </Container>
      <div>DetailScreen</div>
    );
  }
}
