import React from 'react';
import PropTypes from 'prop-types';
// import { View, TouchableHighlight } from 'react-native';
// import { Container, Content, Header, Body, Title, Button, Left, Icon, Right } from 'native-base';
import bindActionCreators from 'redux/lib/bindActionCreators';
import connect from 'react-redux/lib/connect/connect';
import compose from 'recompose/compose';
import pure from 'recompose/pure';

import { reduxForm } from 'redux-form';

import withBackButton from '../components/common/hoc/withBackButton';

import * as TaskActions from '../redux/task/taskActions';
import AddForm from '../components/add/AddForm';
import { addTaskThunk } from '../redux/task/taskMiddleware';
import Util from '../components/common/util/Util';

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(TaskActions, dispatch),
});

const enhance = compose(
  connect(null, mapDispatchToProps),
  reduxForm({ form: 'addForm' }),
  withBackButton(),
  pure,
);

@enhance
export default class AddScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
  };

  getStyles = () => ({
    content: {
      justifyContent: 'space-between',
      padding: 8,
    },
    header: {
      backgroundColor: '#455A64',
    },
    title: {
      color: '#FFFFFF',
    },
    icon: {
      color: '#FFFFFF',
    },
    doneButton: {
      backgroundColor: 'rgba(231,76,60,1)',
      borderColor: 'rgba(231,76,60,1)',
      borderWidth: 1,
      height: 56,
      width: 56,
      borderRadius: 28,
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      bottom: 30,
      right: 30,
      shadowColor: '#000000',
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 0,
      },
    },
    doneButtonIcon: {
      color: 'white',
    },
  });

  goBack = () => this.props.navigation.goBack();

  handleSubmit = () => this.props.handleSubmit(this.submitTask);

  submitTask = (values) => {
    console.log(`AddScreen.submitTask(): values = ${JSON.stringify(values)}`);
    console.log('AddScreen.addTask(): BEFORE dispatch addTask (addTaskThunk)');
    this.props
      .dispatch(
        addTaskThunk({
          name: values.name,
          nameEn: values.nameEn,
          description: values.description,
        }),
      )
      .then(() => {
        console.log('AddScreen.submitTask(): addTask Done!');
        this.props.navigation.goBack();
      })
      .catch((err) => {
        console.log(err);
      });

    console.log('AddScreen.submitTask(): AFTER this.props.dispatch');

    // this.props.navigation.goBack();
  };

  render() {
    const styles = this.getStyles(this.props);

    return (
      // <Container>
      //   <Header style={styles.header}>
      //     <Left>
      //       <Button onPress={this.goBack} transparent>
      //         <Icon name="arrow-back" style={styles.icon} />
      //       </Button>
      //     </Left>
      //     <Body>
      //       <Title style={styles.title}>Создание</Title>
      //     </Body>
      //     <Right />
      //   </Header>
      //   <Content contentContainerStyle={styles.content}>
      //     <AddForm />
      //   </Content>
      //   {Util.platformOS() === 'android' && (
      //     <View>
      //       <TouchableHighlight
      //         style={styles.doneButton}
      //         underlayColor="red"
      //         onPress={this.handleSubmit()}
      //       >
      //         <Icon name="md-checkmark" style={styles.doneButtonIcon} />
      //       </TouchableHighlight>
      //     </View>
      //   )}
      // </Container>
      <div>AddScreen</div>
    );
  }
}
