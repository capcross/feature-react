import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';

export default class FilterFloatingActionButton extends React.Component {
  static propTypes = {
    mainButton: PropTypes.object,
    subButton1: PropTypes.object,
    subButton2: PropTypes.object,
  };

  getStyles = () =>
    StyleSheet.create({
      actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
      },
    });

  render() {
    const styles = this.getStyles(this.props);
    return (
      <ActionButton
        buttonColor="rgba(231,76,60,1)"
        onPress={() => {
          this.props.mainButton.onPress();
        }}
      >
        {this.props.subButton1 && (
          <View>
            <ActionButton.Item
              buttonColor="#9b59b6"
              title={this.props.mainButton.title}
              onPress={() => {
                this.props.mainButton.onPress();
              }}
            >
              <Icon name="md-add" style={styles.actionButtonIcon} />
            </ActionButton.Item>
            <ActionButton.Item
              buttonColor="#3498db"
              title={this.props.subButton1.title}
              onPress={() => {
                this.props.subButton1.onPress();
              }}
            >
              <Icon name="md-bug" style={styles.actionButtonIcon} />
            </ActionButton.Item>
            <ActionButton.Item
              buttonColor="#1abc9c"
              title={this.props.subButton2.title}
              onPress={() => {
                this.props.subButton2.onPress();
              }}
            >
              <Icon name="md-construct" style={styles.actionButtonIcon} />
            </ActionButton.Item>
          </View>
        )}
      </ActionButton>
    );
  }
}
