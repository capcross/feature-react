import React from 'react';
import PropTypes from 'prop-types';
//import { Card, CardItem, Text, Content, Container } from 'native-base';
import { statusItems } from '../../data/constants';

export default class ViewDetail extends React.Component {
  static propTypes = {
    task: PropTypes.object.isRequired,
  };

  getStyles = () => ({
    card: {
      padding: 8,
      borderRadius: 4,
    },
    fieldCaption: {
      fontSize: 15,
      color: '#242424',
    },
    fieldValue: {
      fontSize: 14,
      color: 'gray',
    },
  });

  render() {
    const styles = this.getStyles(this.props);

    const task = this.props.task;

    return (
      // <Container>
      //   <Content>
      //     <Card style={styles.card}>
      //       <CardItem>
      //         <Text style={styles.fieldCaption}>ID</Text>
      //       </CardItem>
      //       <CardItem cardBody>
      //         <Text style={styles.fieldValue}>{task.id}</Text>
      //       </CardItem>
      //     </Card>
      //     <Card style={styles.card}>
      //       <CardItem>
      //         <Text style={styles.fieldCaption}>Автор</Text>
      //       </CardItem>
      //       <CardItem cardBody>
      //         <Text style={styles.fieldValue}>{task.author}</Text>
      //       </CardItem>
      //     </Card>
      //     <Card style={styles.card}>
      //       <CardItem>
      //         <Text style={styles.fieldCaption}>Название</Text>
      //       </CardItem>
      //       <CardItem cardBody>
      //         <Text style={styles.fieldValue}>{task.name}</Text>
      //       </CardItem>
      //     </Card>
      //     <Card style={styles.card}>
      //       <CardItem>
      //         <Text style={styles.fieldCaption}>Название (англ)</Text>
      //       </CardItem>
      //       <CardItem cardBody>
      //         <Text style={styles.fieldValue}>{task.nameEn}</Text>
      //       </CardItem>
      //     </Card>
      //     <Card style={styles.card}>
      //       <CardItem>
      //         <Text style={styles.fieldCaption}>Описание</Text>
      //       </CardItem>
      //       <CardItem cardBody>
      //         <Text style={styles.fieldValue}>{task.description}</Text>
      //       </CardItem>
      //     </Card>
      //     <Card style={styles.card}>
      //       <CardItem>
      //         <Text style={styles.fieldCaption}>Статус</Text>
      //       </CardItem>
      //       <CardItem cardBody>
      //         <Text style={styles.fieldValue}>{task.statusName}</Text>
      //       </CardItem>
      //     </Card>
      //     <Card style={styles.card}>
      //       <CardItem>
      //         <Text style={styles.fieldCaption}>Ответственный</Text>
      //       </CardItem>
      //       <CardItem cardBody>
      //         <Text style={styles.fieldValue}>{task.responsibleName}</Text>
      //       </CardItem>
      //     </Card>
      //   </Content>
      // </Container>
      <div>ViewDetail</div>
    );
  }
}
