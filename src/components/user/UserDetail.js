import React from 'react';
import PropTypes from 'prop-types';
//import { Card, CardItem, Text, Content, Container } from 'native-base';

export default class UserDetail extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
  };

  getStyles = () => ({
    card: {
      padding: 8,
      borderRadius: 4,
    },
    fieldCaption: {
      fontSize: 15,
      color: '#242424',
    },
    fieldValue: {
      fontSize: 14,
      color: 'gray',
    },
  });

  render() {
    const styles = this.getStyles(this.props);

    const user = this.props.user;

    return (
      // <Container>
      //   <Content>
      //     <Card style={styles.card}>
      //       <CardItem>
      //         <Text style={styles.fieldCaption}>ID</Text>
      //       </CardItem>
      //       <CardItem cardBody>
      //         <Text style={styles.fieldValue}>{user.id}</Text>
      //       </CardItem>
      //     </Card>
      //     <Card style={styles.card}>
      //       <CardItem>
      //         <Text style={styles.fieldCaption}>Имя</Text>
      //       </CardItem>
      //       <CardItem cardBody>
      //         <Text style={styles.fieldValue}>{user.name}</Text>
      //       </CardItem>
      //     </Card>
      //   </Content>
      // </Container>
      <div>UserDetail</div>
    );
  }
}
