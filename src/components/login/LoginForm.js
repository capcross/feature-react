import React from 'react';
import { func } from 'prop-types';
//import { Field } from 'redux-form';

// import TextInput from './TextInput';
// import SecureTextInput from './SecureTextInput';
import loginMediator from '../../loginMediator';

//import WaitBar from '../common/WaitBar';

// const styles = StyleSheet.create({
//   input: {
//     textAlign: 'center',
//   },
//   button: {
//     alignItems: 'center',
//   },
//   buttonText: {
//     backgroundColor: 'red',
//     color: 'white',
//     height: 40,
//     lineHeight: 28,
//     textAlign: 'center',
//     alignSelf: 'stretch',
//   },
//   text: {
//     color: 'black',
//     fontSize: 22,
//   },
// });

export default class LoginForm extends React.Component {
  static propTypes = {
    onSubmit: func.isRequired,
  };

  componentDidMount() {
    console.log('LoginForm.componentDidMount()');
    loginMediator.waitBar = this.waitBar; // Для последующего доступа из api.authenticate.
  }

  componentWillReceiveProps(nextProps) {
    console.log(`LoginForm.componentWillReceiveProps(): nextProps = ${JSON.stringify(nextProps)}`);
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log(`LoginForm.shouldComponentUpdate(): nextProps = ${JSON.stringify(nextProps)}`);
    console.log(`LoginForm.shouldComponentUpdate(): nextState = ${JSON.stringify(nextState)}`);
    return true;
  }

  componentWillUpdate(nextProps, nextState) {
    console.log(`LoginForm.componentWillUpdate(): nextProps = ${JSON.stringify(nextProps)}`);
    console.log(`LoginForm.componentWillUpdate(): nextState = ${JSON.stringify(nextState)}`);
  }

  componentDidUpdate() {
    console.log('LoginForm.componentDidUpdate()');
  }

  componentWillUnmount(prevProps, prevState) {
    console.log(`LoginForm.componentWillUnmount(): prevProps = ${JSON.stringify(prevProps)}`);
    console.log(`LoginForm.componentWillUnmount(): prevState = ${JSON.stringify(prevState)}`);
  }

  getClass = object =>
    Object.prototype.toString
      .call(object)
      .match(/^\[object\s(.*)\]$/)[1]
      .toLowerCase();

  render() {
    return (
      // <Container style={{ alignSelf: 'stretch' }}>
      //   <Content contentContainerStyle={{ justifyContent: 'center', marginBottom: 200, flex: 1 }}>
      //     <Form>
      //       <Field
      //         name="username"
      //         style={{ textAlign: 'center' }}
      //         component={TextInput}
      //         // component="input"  type="text"
      //         placeholder="Имя"
      //         ref={(c) => {
      //           this.username = c;
      //         }}
      //       />
      //       <Field
      //         name="password"
      //         style={{ textAlign: 'center' }}
      //         component={SecureTextInput}
      //         placeholder="Пароль"
      //         ref={(c) => {
      //           this.password = c;
      //         }}
      //       />
      //       <TouchableOpacity
      //         style={styles.button}
      //         onPress={() => {
      //           console.log('LoginForm...onPress()');
      //           this.props.onSubmit();
      //         }}
      //       >
      //         <Text style={styles.buttonText}>Войти</Text>
      //       </TouchableOpacity>
      //     </Form>

      //     <WaitBar
      //       ref={(c) => {
      //         this.waitBar = c;
      //       }}
      //     />
      //   </Content>
      // </Container>
      <div>LoginForm</div>
    );
  }
}
