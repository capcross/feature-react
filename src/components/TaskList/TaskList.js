import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { reduxForm } from 'redux-form';
import 'react-table/react-table.css'
import Modal from '../common/Modal';
import LoginForm from '../login/LoginForm';
import loginMediator from '../../loginMediator';

import { findTaskThunk } from '../../redux/task/taskMiddleware';
import { getUserData } from '../../redux/user/userMiddleware';
import log from '@cap-cross/cap-core';
import ListForm from '../common/ListForm';

//import { jepFetch } from '../../api/loginApiImpl'; // TODO убрать после отладки


const enhance = compose(
  reduxForm({ form: 'loginForm' }),
);

let wasFetched = false; // Признак необходимости первоначального fetch

@enhance
export default class TaskList extends React.Component {
  static propTypes = {
    items: PropTypes.array.isRequired,
    filter: PropTypes.object.isRequired,
    receiveTasks: PropTypes.func.isRequired,
    removeTask: PropTypes.objectOf(PropTypes.func),
  };

  // Для Login-диалога (Modal)
  state = {
    isLoginPending: false,
  };

  componentDidMount() {
    loginMediator.dialogForm = this;
    wasFetched = false; // TODO разобраться с времянкой!
    //if (this.props.user.operatorId === '') this.fetchUser();
    this.fetchUser();
    if (!wasFetched) {
      this.fetchData(this.props.filter, this.props.receiveTasks);
      wasFetched = true;
    }

    this.fetchData(this.props.filter, this.props.receiveTasks);
  }


  componentDidUpdate(prevProps) {
    if (this.props.filter !== prevProps.filter) {
      this.fetchData(this.props.filter, this.props.receiveTasks);
    }
  }


  onModalLoginClose = () => {
    // TODO Почему не вызывается ?
    alert('Modal has been closed.'); // eslint-disable-line no-alert
  };

  setLoginPending(visible) {
    this.setState({ isLoginPending: visible });
  }

  // eslint-disable-next-line
  getStyles = props => ({
    // content: {
    //     justifyContent: 'space-between',
    //     padding: 8
    // }
  });

  loginOpen() {
    this.setLoginPending(true);
  }

  loginClose() {
    this.setLoginPending(false);
    this.fetchData(this.props.filter, this.props.receiveTasks); // Выпрямить!!!
  }

  fetchData = (filter, receiveTasks) => {
    this.props
      // eslint-disable-next-line react/prop-types
      .dispatch(findTaskThunk(filter, receiveTasks, this.waitwaitBar))
      .then(() => {
      })
      .catch((err) => {
        log.trace(err.message);
        // Toast.show({
        //   text: err.message,
        //   type: 'danger',
        //   buttonText: 'OK',
        //   duration: 5000
        // });
      });
  };

  fetchUser = () => {
    this.props
      // eslint-disable-next-line react/prop-types
      .dispatch(getUserData());
  }

  handleSubmit = () => this.props.handleSubmit(loginMediator.onLoginSubmit);

  modalLoginView = (
    // <JwModal
    //   animationType="slide"
    //   transparent={false}
    //   visible={this.state.isLoginPending}
    //   onRequestClose = {() => {
    //     this.onModalLoginClose();
    //   }}
    //   onShow = {() => {
    //   }}
    //   ref={(c) => {
    //     this.modalLoginDialog = c;
    //   }}
    // >
    //   <LoginForm onSubmit={this.handleSubmit()} />
    // </JwModal>
    <Modal
      render={
        <LoginForm onSubmit={this.handleSubmit()} />
      }
      visible={this.state.isLoginPending}
      onRequestClose = {() => {
        this.onModalLoginClose();
      }}
      onShow = {() => {
      }}
      ref={(c) => {
        this.modalLoginDialog = c;
      }}/>
  );


  render() {
    log.trace('TaskList.render: BEGIN');

    const items = this.props.items;
    log.trace('TaskList.render: items = ' + items);

    // TODO переместить во что-то типа ListConfig
    const columns = [{
      Header: 'Id',
      accessor: 'id'
    }, {
      Header: 'Автор',
      accessor: 'author',
      //Cell: props => <span className='number'>{props.value}</span> // Custom cell components!
    }, {
      Header: 'Название',
      accessor: 'name'
    }, {
      Header: 'Название (англ)',
      accessor: 'nameEn'
    }, {
      Header: 'Описание',
      accessor: 'description'
    }, {
      Header: 'Статус',
      accessor: 'status'
    }];

    return <ListForm
      columns={columns}
      data={items}
    />;
  }
}
