import React from 'react';
import PropTypes from 'prop-types';
//import { Card, CardItem, Text, Left, Right } from 'native-base';
import {setActiveTask} from '../../redux/task/taskActions'
import { connect } from 'react-redux';
import log from '@cap-cross/cap-core';

class TaskItem extends React.Component {
  static propTypes = {
    task: PropTypes.object.isRequired,
    //navigation: PropTypes.object.isRequired,
    remove: PropTypes.func.isRequired,
  };

  getStyles = props => ({
    card: {
      borderRadius: 4,
    },
    button: {
      marginTop: 3,
    },
    text: {
      color: props.color,
      marginLeft: 8,
    },
  });

  goToDetailViewTask = (task) => {
    // TODO Убедиться, что нет лучшего решения
    task.remove = this.props.remove; // eslint-disable-line no-param-reassign
    this.props.setActiveTask(task);

    //this.props.navigation.navigate('ViewTask');
    log.warn('TaskItem.goToDetailViewTask: переход не реализован');
  };

  render() {
    const styles = this.getStyles(this.props);

    return (
      // <Card style={styles.card}>
      //   <CardItem button onPress={() => this.goToDetailViewTask(this.props.task)}>
      //     <Left>
      //       <Text>{this.props.task.name}</Text>
      //     </Left>
      //     <Right>
      //       <Text>{this.props.task.author}</Text>
      //     </Right>
      //   </CardItem>
      // </Card>
      <div style={styles.card}>TaskItem</div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setActiveTask: (task) => dispatch(setActiveTask(task))
  };
}

export default connect(null, mapDispatchToProps)(TaskItem);
