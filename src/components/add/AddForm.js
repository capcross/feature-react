import React from 'react';
//import { Form } from 'native-base';
import { Field } from 'redux-form';
import {required, expected} from '../../data/validation';
// import {
//   StyleSheet,
//   View,
// } from 'react-native';

import TextInput from '../common/TextInput';
import TextArea from '../common/TextArea';

export default class AddForm extends React.Component {

  render() {
    console.log('AddForm.render()');
    const styles = StyleSheet.create({
      formfields: {
          paddingTop: 12,
          paddingBottom: 12,
      },
    });

    return (
      <div>
        <Field
          style={styles.formfields}
          name="name"
          component={TextInput}
          labelText="Название"
          validate = {required}
        />
        <Field
          style={styles.formfields}
          name="nameEn"
          component={TextInput}
          labelText="Название (англ)"
          validate = {required}
        />
        <Field
          style={styles.formfields}
          name="description"
          component={TextArea}
          labelText="Описание" 
          warn = {expected}
        />
      </div>
    );
  }
}
