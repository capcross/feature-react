import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from "react-table";
import EmptyView from '../common/EmptyView';

export default class ListForm extends React.Component {
    static propTypes = {
        columns: PropTypes.array.isRequired,
        data: PropTypes.array.isRequired,
        // filter: PropTypes.object.isRequired,
        // receiveTasks: PropTypes.func.isRequired,
        removeTask: PropTypes.objectOf(PropTypes.func),
//        handleSubmit: PropTypes.func.isRequired,
      };

    state = {
        selected: null
    };

    render() {
        let contentView = <EmptyView text="Нет задач" />; // eslint-disable-line react/prop-types

        const items = this.props.data;

        if (items.length > 0) {
          contentView = (
            <ReactTable
        
              getTrProps = {(state, rowInfo) => {
                return {
                  onDoubleClick: (e) => {
                    this.setState({
                      selected: rowInfo.index
                    })
                  },
                  onClick: (e) => {
                    this.setState({
                      selected: rowInfo.index
                    })
                  },
                  style: {
                    background: rowInfo && rowInfo.index === this.state.selected ? '#00afec' : 'white',
                    color: rowInfo && rowInfo.index === this.state.selected ? 'white' : 'black'
                  }
                }
              }}
        
              data={items}
              columns={this.props.columns}
            />
        
          );
        }
  
      const result = (
        <div>
          {contentView}
        </div>
      );
  
      return result;
    }
}
