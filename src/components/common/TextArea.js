// TODO Устранить дублирование с LoginForm
import React from 'react';
import PropTypes from 'prop-types';
// import { Card, Input, Item, Label, Textarea } from 'native-base';
// import { Text } from 'react-native';

export default class TextInput extends React.PureComponent {
  static propTypes = {
    input: PropTypes.object.isRequired,
    meta: PropTypes.object.isRequired,
  };

  render() {
    const { input, meta: { touched, error, warning}, labelText, ...inputProps } = this.props;
    // TODO multiline, numberOfLines введены только для работы в Expo (проверить)
    // На Android при этом приходится вертикально скроллить, если содержимое не вмещается в
    // отведённое число строк. На iPhone всё нормально - отображается всё содержимое.
    // Это искажает отображение пустых форм - поля располагаются слишком близко - исправить для Expo
    var notificationStyle = error !== undefined ? {color: 'red'} : (warning !== undefined ? {color : 'gray'} : {});
    return (
      // <Card>
      //   <Item style={{flexDirection:'row'}} error={touched && error !== undefined}>
      //     <Label>{labelText}:</Label>
      //       <Textarea
      //         rowSpan={15}
      //         onChangeText={input.onChange}
      //         onBlur={input.onBlur}
      //         onFocus={input.onFocus}
      //         value={input.value}
      //         {...inputProps}
      //         style={{ flex:1, flexWrap:'wrap', fontSize:17}}
      //       />
      //       {touched && <Text style={{...notificationStyle, padding: 5}}>{error !== undefined ? error : (warning !== undefined ? warning : undefined)}</Text>}
      //   </Item>
      // </Card>
      <div>TextArea</div>
    );
  }
}
