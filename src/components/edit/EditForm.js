import React from 'react';
import PropTypes from 'prop-types';
// import { Form } from 'native-base';
// import {StyleSheet, View} from 'react-native';
import { Field, reduxForm } from 'redux-form';

import TextArea from '../common/TextArea';
import TextInput from '../common/TextInput';
import StatusPicker from '../common/StatusPicker';
import OperatorPicker from '../common/OperatorPicker';
import isUserHaveRoles from '../../data/clientSecurity';
import {required, expected} from '../../data/validation';
import { getStatuses } from '../../redux/status/statusMiddleware';
import { getOperators } from '../../redux/operator/operatorMiddleware';
import { connect } from 'react-redux';

class EditForm extends React.Component {

  componentDidMount() {
    this.props.getStatuses();
    this.props.getOperators();
  }
  
  //handleSubmit = () => this.props.handleSubmit(this.submitTask);

  render() {
    console.log('EditForm!.render() BEGIN');
    console.log(`EditForm!: task = ${JSON.stringify(this.props.initialValues)}`);

    const styles = StyleSheet.create({
      formfields: {
          paddingTop: 12,
          paddingBottom: 12,
      },
    });

    let task = this.props.task;
    let isJrsAssignResponsibleFeatureRole = this.props.userRoles.length > 0 ? isUserHaveRoles(["JrsAssignResponsibleFeature"], this.props.userRoles) : false;
    
    return (
          // <Form>
          //   <Field 
          //     style={styles.formfields}
          //     name="name"
          //     component={TextInput}
          //     labelText="Название"
          //     validate = {required}
          //   />
          //   <Field
          //     style={styles.formfields}
          //     name="nameEn"
          //     component={TextInput}
          //     labelText="Название (англ)"
          //     validate = {required}
          //   />
          //   <Field
          //     style={styles.formfields}
          //     name="description"
          //     component={TextArea}
          //     labelText="Описание" 
          //     warn = {expected}
          //   />
          //   <Field
          //     style={styles.formfields}
          //     name="statusCode"
          //     component={StatusPicker}
          //     labelText="Статус"
          //     items={this.props.statuses}
          //   />
          //   { isJrsAssignResponsibleFeatureRole && //проверка наличия роли
          //     <Field
          //       style={styles.formfields}
          //       name="responsible"
          //       component={OperatorPicker}
          //       labelText="Ответственный"
          //       items={this.props.operators}
          //     />
          //   }
          // </Form>
          <div>EditForm</div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userRoles: state.user.userRoles,
    operators: state.operators,
    statuses: state.statuses,
    isLoading: state.isLoading,
    isFailed: state.isFailed,
    errorMessage: state.errorMessage,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getStatuses: () => dispatch(getStatuses()),
    getOperators: () => dispatch(getOperators())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditForm);//(reduxForm({form: "EditForm"})(EditForm));