// loginMedianor.js
// Используется для слабосвязанной интеграции компонента с LoginForm с функционалом аутентификации
// TODO Перенести состояние в Redux

const loginMediator = {
  dialogForm: undefined,

  waitBar: undefined,

  // Слушатель LoginForm submit со стороны функционала аутентификации
  loginSubmitListener: undefined,

  isLoginFormOpened: false, // Поскольку isOpen не работает

  isWaitBarOpened: false,

  openLoginForm() {
    console.log(
      `loginMediator.openLoginForm(): this.isLoginFormOpened = ${this.isLoginFormOpened}`,
    );

    if (!this.isLoginFormOpened) {
      this.dialogForm.loginOpen();
      this.isLoginFormOpened = true;
      console.log('loginMediator.openLoginForm() - Done');
    } else {
      console.log('loginMediator.openLoginForm() - Was already opened');
    }
  },

  closeLoginForm() {
    console.log(
      `loginMediator.closeLoginForm(): this.isLoginFormOpened = ${this.isLoginFormOpened}`,
    );

    if (this.isLoginFormOpened) {
      this.dialogForm.loginClose();
      this.isLoginFormOpened = false;
      console.log('loginMediator.closeLoginForm() - Done');
    }
  },

  openAuthRequestWaitBar() {
    console.log(
      `loginMediator.openAuthRequestWaitBar(): this.isWaitBarOpened = ${this.isWaitBarOpened}`,
    );
    if (!this.isWaitBarOpened) {
      this.waitBar.open();
      this.isWaitBarOpened = true;
      console.log('loginMediator.openAuthRequestWaitBar() - Done ');
    }
  },
  closeAuthRequestWaitBar() {
    console.log(
      `loginMediator.closeAuthRequestWaitBar(): this.isWaitBarOpened = ${this.isWaitBarOpened}`,
    );
    if (this.isWaitBarOpened) {
      this.waitBar.close();
      this.isWaitBarOpened = false;
      console.log('loginMediator.closeAuthRequestWaitBar() - Done ');
    }
  },

  // Callback для LoginForm submit
  onLoginSubmit: (credentials) => {
    console.log(`loginMediator.onLoginSubmit() BEGIN: credentials = ${credentials}`);
    const lsListener = loginMediator.loginSubmitListener;
    console.log(`loginMediator.onLoginSubmit(): lsListener = ${lsListener}`);

    if (lsListener) {
      lsListener(credentials);
    } else {
      throw new Error('loginSubmitListener should be set');
    }

    console.log(`loginMediator.onLoginSubmit() END`);
  },
};

module.exports = loginMediator;
