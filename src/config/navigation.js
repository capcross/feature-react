// import { createStackNavigator, createDrawerNavigator } from 'react-navigation';
// import {
//   reduxifyNavigator,
//   createReactNavigationReduxMiddleware,
// } from 'react-navigation-redux-helpers';
// import { connect } from 'react-redux';


// import ListScreen from '../screens/ListScreen';
// import AddScreen from '../screens/AddScreen';
// import DetailScreen from '../screens/DetailScreen';
// import EditScreen from '../screens/EditScreen';
// import FilterScreen from '../screens/FilterScreen';

// import UserScreen from '../screens/UserScreen';

// const mapStateToProps = state => ({
//   state: state.navigate,
// });

// const middleware = createReactNavigationReduxMiddleware(
//   'root',
//   state => state.navigate
// );

// const taskNavigator = createStackNavigator(
// {
//     Home: {screen: ListScreen},
//     ViewTask: {screen: DetailScreen},
//     AddTask: {screen: AddScreen},
//     EditTask: {screen: EditScreen},
//     FilterTasks: {screen: FilterScreen},
// },
// {
//   initialRouteName: 'Home',
//    headerMode: 'none' }
// );

// const RootNavigator = createDrawerNavigator(
// {
//   "Профиль": {screen: UserScreen},
//   "Задачи": {screen: taskNavigator}
// },
// {
//   initialRouteName: 'Задачи',
//   headerMode: 'none'
// });

// const AppWithNavigationState = reduxifyNavigator(RootNavigator, 'root');

// const Navigator = connect(mapStateToProps)(AppWithNavigationState);

// export { RootNavigator, Navigator, middleware };
