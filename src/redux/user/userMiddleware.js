import * as actions from './userActions.js';
import {FEATURE_CONTEXT_URL} from '../../api/apiConfig';
import { jepFetch } from '../../api/loginApiImpl';

const USER_DATA_API_URL = `${FEATURE_CONTEXT_URL}/userdata`;

export const getUserData = () => {
    return (dispatch) => {
      console.log(`getUserData(): BEGIN`);
      dispatch(actions.fetchUser(true));
      console.log("fetching getUserData():" + USER_DATA_API_URL);
      jepFetch(USER_DATA_API_URL)
        .then((response) => {
          dispatch(actions.fetchUserSuccess(response.body));
        })
        .catch((error) => {
          console.log(`getUserData().then.fetch(${USER_DATA_API_URL}).catch: fetch error = ${error}`);
          dispatch(actions.fetchUserFailure(true, error.message));
        });

      // TODO Убрать после отладки, вернуть закомментаренное
      //debug(dispatch);
      };
  }

  const debug = dispatch => dispatch(actions.fetchUserSuccess({                    
    userName: "Test User",
    operatorId: "5",
    userRoles: [],
  }));