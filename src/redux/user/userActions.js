export const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';

export function fetchUser(isLoading) {
    console.log("FETCH featureUser BEGIN " + isLoading);

    return {
        type: FETCH_USER_BEGIN,
        payload : {
            isLoading,
        }
    }
}

export function fetchUserSuccess(user) {
    // TODO fake data - Убрать после отладки
    user = {                    
        userName: "Test User",
        operatorId: "5",
        userRoles: [],
    };
    
    console.log("FETCH featureUser SUCCESS " + JSON.stringify(user));

    return {
        type: FETCH_USER_SUCCESS,
        payload : {
            user,
        }
    }
}


export function fetchUserFailure(isFailed, errorMessage) {
    console.log("FETCH featureUser FAILURE " + isFailed + " " + errorMessage);

    return {
        type: FETCH_USER_FAILURE,
        payload : {
            isFailed,
            errorMessage,
        }
    }
}
