import {combineReducers} from 'redux';
import { reducer as form } from 'redux-form';

import task from './task/taskReducer';
//import navigate from './navigate/reducer';
import operators from './operator/operatorReducer';
import statuses from './status/statusReducer';
import user from './user/userReducer';

export default combineReducers({
  task,
//  navigate,
  form,
  operators,
  statuses,
  user
});
