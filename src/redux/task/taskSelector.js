import { createSelector } from 'reselect';

const getFilter = state => state.task.filter;
const getTasks = state => state.task.items;

function getByFilter(filter, items) {
  return (
    !filter ||
    items.filter(
      item =>
        (!filter.name || item.name === filter.name) &&
        (!filter.description || item.description === filter.description),
    )
  );
}

const getVisibleTasks = createSelector([getFilter, getTasks], getByFilter);

export default getVisibleTasks;
// export const getVisibleTasks = createSelector([getFilter, getTasks], getByFilter);
