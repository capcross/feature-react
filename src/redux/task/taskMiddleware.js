import {tasks} from '../../api/FeatureAPI';
import fakeTaskItems from '../../data/fakeTaskItems';  // TODO Убрать после отладки

export const findTaskThunk = (filter, receiveTasks, waitBar) => {
  console.log(`taskMiddleware.findTask() = ${JSON.stringify(filter)}`);

  return (dispatch) => {
    console.log(`taskMiddleware.find.dispatch()=${dispatch}`);

    /*  eslint no-unused-expressions: ["error", { "allowShortCircuit": true }]  */
    waitBar && waitBar.open();
    return tasks
      .find(filter)
      .then(({ response, body }) => {
        if (!response.ok) {
          throw new Error('Network response was not ok.');
        }
        body && receiveTasks(filter, body);
        waitBar && waitBar.close();
        return body;
      })
      .catch((error) => {
        console.log('taskMiddleware-find error', error);
        waitBar && waitBar.close();
      });

      // TODO Убрать после отладки, вернуть закомментаренное
      // receiveTasks(filter, fakeTaskItems);
      // return Promise.resolve(fakeTaskItems);
    };

};

export const addTaskThunk = (task) => {
  console.log(`taskMiddleware.addTask() = ${JSON.stringify(task)}`);
  return (dispatch) => {
    console.log(`taskMiddleware.addTaskThunk.dispatch()=${dispatch}`);
    return tasks
      .addTask(task)
      .then((response) => {
        // TODO Убрать лишнее
        console.log(`taskMiddleware.addTask().then: response=${response}`);
      })
      .catch((error) => {
        console.log('taskMiddleware-add error', error);
      });
  };
};

export const updateTaskThunk = (task) => {
  console.log(`taskMiddleware.updateTask() = ${JSON.stringify(task)}`);
  return (dispatch) => {
    console.log(`taskMiddleware.updateTaskThunk.dispatch()=${dispatch}`);

    return tasks
      .updateTask(task)
      .then((response) => {
        // TODO Убрать лишнее
        console.log(`taskMiddleware.updateTask().then: response=${response}`);
      })
      .catch((error) => {
        console.log("EERRRRROOOOOOOOOOOOOOOOOOOR " + JSON.stringify(error));
        console.log('taskMiddleware-update error', error);
      });
  };
};

export const logoutThunk = (task) => {
  console.log(`taskMiddleware.removeTask() = ${JSON.stringify(task)}`);
  return (dispatch) => {
    console.log(`taskMiddleware.removeTaskThunk.dispatch()=${dispatch}`);

    return tasks
      .removeTask(task)
      .then((response) => {
        // TODO Убрать лишнее
        console.log(`taskMiddleware.removeTask().then: response=${response}`);
      })
      .catch((error) => {
        console.log('taskMiddleware-remove error', error);
      });
  };
};
