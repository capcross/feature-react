//import fakeTaskItems from '../../data/fakeTaskItems';  // TODO РЈР±СЂР°С‚СЊ РїРѕСЃР»Рµ РѕС‚Р»Р°РґРєРё
import log from '@cap-cross/cap-core';

export const RECEIVE_TASKS = 'RECEIVE_TASKS';
export const ADD_TASK = 'ADD_TASK';
export const UPDATE_TASK = 'UPDATE_TASK';
export const FILTER_TASK = 'FILTER_TASK';
export const SHOW_TASKLIST = 'SHOW_TASKLIST';
export const TOGGLE_TASK = 'TOGGLE_TASK';
export const REMOVE_TASK = 'REMOVE_TASK';
export const CHANGE_FILTER = 'CHANGE_FILTER';
export const ACTIVE_TASK = 'ACTIVE_TASK';

const createAction = object => dispatch => dispatch(object);

export function receiveTasks(filter, items) {
  log.trace('actions.js: receiveTasks() items = ' + JSON.stringify(items));

//  items = fakeTaskItems;  // TODO РЈР±СЂР°С‚СЊ РїРѕСЃР»Рµ РѕС‚Р»Р°РґРєРё

  return createAction({
    type: RECEIVE_TASKS,
    payload: {
      filter,
      items,
    },
  });
}

export function changeFilter(filter) {
  return createAction({
    type: CHANGE_FILTER,
    payload: {
      filter,
    },
  });
}

export function setActiveTask(activeItem) {
  console.log("ACTIVE TASK " + JSON.stringify(activeItem));
  return {
    type: ACTIVE_TASK,
    payload: {
      activeItem,
    },
  };
}

// TODO Реализовать закомментаренный функционал через Redux (продумать как)
// export function removeTask(id) {
//     return createAction({
//         type: REMOVE_TASK,
//         payload: {
//             id
//         }
//     });
// }

// export function addTask(task) {
//     return createAction({
//         type: ADD_TASK,
//         payload: {
//             task
//         }
//     });
// }

// export function updateTask(task) {
//     console.log('actions.js.updateTask()');
//     return createAction({
//         type: UPDATE_TASK,
//         payload: {
//             task
//         }
//     });
// }
