import * as TaskActions from './taskActions';

const initialState = {
  filter: {},
  items: [],
  activeItem: {},
};

export default function reducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case TaskActions.RECEIVE_TASKS: {
      console.log('Task reducer: RECEIVE_TASKS');
      const stateResult = {
        filter: payload.filter,
        items: payload.items,
        activeItem: {},
      };
      return stateResult;
    }

    case TaskActions.CHANGE_FILTER:
      return {
        ...state,
        filter: payload.filter,
      };
    
    case TaskActions.ACTIVE_TASK:
      return {
        ...state,
        activeItem: payload.activeItem,
      }

    default:
      return state;
  }
}
