// FeatureAPI.js
import * as apiConfig from './apiConfig';
import { jepFetch } from './loginApiImpl';

const buildFindUrl = (filter) => {
  // TODO Реализовать посерьёзнее
  let findUrl = apiConfig.FEATURE_API_FIND_URL;
  if (filter.id || filter.name || filter.nameEn || filter.description) {
    findUrl += '?';
  }
  let first = true;
  if (filter.id) {
    findUrl += 'id=';
    findUrl += filter.id;
    first = false;
  }
  if (filter.name) {
    if (!first) {
      findUrl += '&';
    }
    findUrl += 'name=';
    findUrl += filter.name;
    first = false;
  }
  if (filter.nameEn) {
    if (!first) {
      findUrl += '&';
    }
    findUrl += 'nameEn=';
    findUrl += filter.nameEn;
    first = false;
  }
  if (filter.description) {
    if (!first) {
      findUrl += '&';
    }
    findUrl += 'description=';
    findUrl += filter.description;
  }

  console.log(`FeatureAPI.buildFindUrl(): findUrl = ${findUrl}`);
  return findUrl;
};

const features = {
  find(filter) {
    return jepFetch(buildFindUrl(filter));
  },

  addTask(task) {
    return jepFetch(apiConfig.FEATURE_API_ADD_URL, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify({
        name: task.name,
        nameEn: task.nameEn,
        description: task.description,
      }),
    })
      .then(({ response, body }) => {
        if (!response.ok) {
          throw new Error('Network response was not ok.');
        }

        return body;
      })
      .catch((error) => {
        throw error;
      });
  },

  updateTask(task) {
    const updateTaskUrl = apiConfig.FEATURE_API_UPDATE_URL;
    return jepFetch(updateTaskUrl, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify({
        id: task.id,
        name: task.name,
        nameEn: task.nameEn,
        description: task.description,
        statusCode: task.statusCode,
      }),
    })
      .then(({ response, body }) => {
        if (!response.ok) {
          throw new Error('Network response was not ok.');
        }

        return body;
      })
      .catch((error) => {
        throw error;
      });
  },

  removeTask(task) {
    const removeTaskUrl = apiConfig.FEATURE_API_DELETE_URL;
    return jepFetch(`${removeTaskUrl}/${task.id}`, {
      method: 'DELETE',
    })
      .then(({ response, body }) => {
        if (!response.ok) {
          console.log(`FeatureAPI.fetch.then: response.status = ${response.status}`);
          throw new Error('Network response was not ok.');
        }
      })
      .catch((error) => {
        throw error;
      });
  },
};

// TODO Реализовать загрузку статусов
  const statuses = {  
    getStatuses() {
      return jepFetch(apiConfig.FEATURE_STATUSES_URL);
    }
  };

  const operators = {  
    getOperators() {
      return jepFetch(apiConfig.FEATURE_OPERATORS_URL);
    },
  };

export {features as tasks, statuses, operators};