// loginApiImpl.js - реализация loginApi
import axios from 'axios';

// import { configureJepFetch, fetchJSON } from '../'; // from 'jep-fetch'
import fetchJSON from './fetchJSON'; // from 'jep-fetch'
import configureJepFetch from './configureJepFetch';

import loginMediator from '../loginMediator';
//import {FEATURE_CONTEXT_URL} from './apiConfig';
//import * as UserActions from '../redux/user/userActions';

const SSO_CONTEXT_PART = 'SsoUi'; // Может быть также SsoUi_XX

//const USER_DATA_API_URL = `${FEATURE_CONTEXT_URL}/userData`;

const isAuthorizationError = error => error.message.indexOf("Failed to fetch") > -1;

const encodedParams = (params) => {
  const esc = encodeURIComponent;
  return Object.keys(params)
    .map(k => `${esc(k)}=${esc(params[k])}`)
    .join('&');
};

const shouldAuthenticate = error => isAuthorizationError(error);

export const isSsoLoginRequest = (response) => {
  console.log(`loginApiImpl.isSsoLoginRequest(): response.status = ${response.status}`);
  console.log(`loginApiImpl.isSsoLoginRequest(): response.url = ${response.url}`);

  const result = response.status === 200 && response.url.indexOf(SSO_CONTEXT_PART) > -1;
  console.log(`loginApiImpl.isSsoLoginRequest() = ${result}`);
  return result;
};

const getCredentials = () =>
  new Promise((resolve) => {
    // "Подписка" (установка callback) на loginForm submit
    loginMediator.loginSubmitListener = (credentials) => {
      // loginMediator.closeLoginForm();
      resolve(credentials);
    };

    loginMediator.openLoginForm();
  });


const AUTOLOGON_URL = `http://localhost:8080/feature-json-jepria-backend/LoginServlet?username=nagornyys&password=123`;
const autologonRequestConfig = {
  method: 'GET',
  credentials: 'include',
};
  
//const authenticate = async (ssoContextUrl) => {
const authenticate = (ssoContextUrl) => {
    console.log(`loginApiImpl.authenticate(${ssoContextUrl}) BEGIN`);

  return getCredentials()
    .then(({ username, password }) => {
      const params = {
        j_username: username,
        j_password: password,
      };
      const authenticationUrl = `${ssoContextUrl}/j_security_check?${encodedParams(params)}`;

      console.log(`loginApiImpl.authenticate(): authenticationUrl = ${authenticationUrl}`);
      //loginMediator.openAuthRequestWaitBar();
      return fetch(authenticationUrl, {
        method: 'POST',
        credentials: 'include',
      })
      .then((response) => {
        console.log(`loginApiImpl.authenticate(): response = ${JSON.stringify(response)}`);
        //loginMediator.closeAuthRequestWaitBar();
        loginMediator.closeLoginForm();
        return response;
      })
      .catch((error) => {
        console.log(
          `loginApiImpl.getCredentials().then.fetch(${authenticationUrl}).catch: fetch error = ${
            error
          }`,
        );
        //loginMediator.closeAuthRequestWaitBar();
        throw error;
      });
    })
    .catch((error) => {
      console.log(`loginApiImpl.getCredentials().then.catch(): error = ${error}`);
      throw error;
    });

  // try {
  //   console.log('Test Axios: BEFORE autologon, URL = ' + AUTOLOGON_URL);
  //   const autlogonResponse = await axios(AUTOLOGON_URL, autologonRequestConfig);
  //   console.log('Test Axios: AFTER autologon');
  //   console.log('Test Axios: autlogonResponse = ' + JSON.stringify(autlogonResponse));
  // } catch (error) {
  //   //return () => { throw error; };
  //   console.log(`error = ${error}`);
  // }    
};

export const jepFetch = configureJepFetch({
  shouldAuthenticate,
  authenticate,
  fetch: fetchJSON,
});
