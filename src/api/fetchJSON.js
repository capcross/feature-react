/* global fetch */
// import "isomorphic-fetch" // Только для web!

import merge from 'lodash/merge';
import * as security from './loginApiImpl';

const checkStatus = ({ response, body }) => {
  console.log('fetchJSON.checkStatus()');
  if (security.isSsoLoginRequest(response)) {
    // TODO Надо как-нибудь выпрямить (передать url более естественным образом)
    const error = new Error(response.url);

    console.log(`fetchJSON.checkStatus() error = ${error}`);
    throw error;
  } else if (response.ok) {
    console.log('fetchJSON.checkStatus() success');
    return { response, body };
  } else {
    const error = new Error(response.statusText);
    console.log(`fetchJSON.checkStatus() error${error}`);
    error.response = response;
    error.body = body;
    throw error;
  }
};

const parseJSON = (response) => {
  console.log('fetchJSON.parseJSON(): status = ' + response.status);
  const isJsonResponse =
    response.headers.get('content-type') &&
    response.headers
      .get('content-type')
      .toLowerCase()
      .indexOf('application/json') >= 0;

  if (isJsonResponse) {
    return response.json().then(body => ({ response, body }));
  }
  return Promise.resolve({ response, body: null });
};

const fetchJSON = (input, init) => {
  const initJson = merge(
    {
      credentials: 'include',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    },
    init,
  );

  console.log(`fetchJSON() initJson = ${JSON.stringify(initJson)}`);

  return fetch(input, initJson)
    .then(parseJSON)
    .then(checkStatus);
};

export default fetchJSON;
