function configureJepFetch(loginApi = {}) {
  const { authenticate, shouldAuthenticate, fetch } = loginApi;

  let authenticationPromise = null;

  const extractContext = url =>
    url.substr(0, url.indexOf('/', url.indexOf('/', url.indexOf('//') + 2) + 1));

  return (input, init) => {

    // try {
    //   console.log('BEFORE fetch 1');
    //   fetch( 
    //     "http://msk-dit-20597:8080/feature-json-jepria-backend/LoginServlet?username=nagornyys&password=123",
    //     {
    //       method:'GET',
    //       credentials: 'include'
    //     })
    //     .then((auth) => {
    //       console.log("Fetch_1: auth: " + auth);
    //       console.log("BEFORE Fetch_2");
    //       fetch( 
    //         "http://msk-dit-20597:8080/feature-json-jepria-backend/v1/features",
    //         {
    //           method:'GET',
    //           credentials: 'include',
    //           headers: {
    //             'Accept': 'application/json',
    //             'Access-Control-Request-Method': 'GET',
    //             'Access-Control-Request-Headers': 'Content-Type',
    //             'Content-Type': 'application/json'
    //           }
    //         })
    //         .then((response) => {
    //         console.log("Fetch_2: RESPONSE: " + JSON.stringify(response))
    //         response.json().then(function(data) {
    //           console.log("Fetch_2: response.json().then")
    //           document.getElementById("response").innerHTML = JSON.stringify(data);
    //         });
    //         });
    //     });
    // } catch (error) {
    //   //return () => { throw error; };
    //   console.log(`error = ${error}`);
    // }

    
    return fetch(input, init).catch((error) => {
      console.log('configureJepFetch: error = ' + JSON.stringify(error));
      if (shouldAuthenticate(error)) {
        // TODO Передать url более естественным образом
        if (authenticationPromise === null) {
          authenticationPromise = new Promise((resolve, reject) => {
            authenticate()
              .then(() => {
                console.log('configureJepFetch: AFTER authenticate...');
                authenticationPromise = null;
                resolve();
              })
              .catch((authenticationError) => {
                authenticationPromise = null;
                reject(authenticationError);
              });
          });
        }

        return authenticationPromise
          .then(() => fetch(input, init)).catch(() => {
          // If authentication fails, continue with original error
          throw error;
        });
      }
      throw error;
    });
  };
}

export default configureJepFetch;
