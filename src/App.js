import React from 'react';
import configureStore from './config/store';
import DataProvider from './data/DataProvider';
import ListScreen from './screens/ListScreen';

export default class FeatureApp extends React.Component {

  render() {
    return (
      <DataProvider store={configureStore()} >
        <ListScreen />
      </DataProvider>
    );
  }
}
