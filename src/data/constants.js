export const statusItems = [
  {
    statusName: "Новый",
    statusCode: "NEW"
  },
  {
    statusName: "Установлен порядок выполнения",
    statusCode: "SEQUENCED"
  },
  {
    statusName: "Назначен",
    statusCode: "ASSIGNED"
  },
  {
    statusName: "В работе",
    statusCode: "INPROGRESS"
  },
  {
    statusName: "Доработки по замечаниям",
    statusCode: "REMARKS"
  },
  {
    statusName: "Реализован",
    statusCode: "DONE"
  },
  {
    statusName: "Отменëн",
    statusCode: "CANCELLED"
  },
];